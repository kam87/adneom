# README #

Le projet Adneom contient le code de sources et les tests.
Pour utiliser la fonction de partition, il faut appeler la méthode static partition(List,int) implémenté dans la classe CollectionsUtils qui se trouve dans le package src/com/adneom/util.
Afin d'effectuer les tests avec Junit4, il faut lancer la class CollectionsUtilsTest (clique droit → Run as → Junit test) dans le package src/com/adneom /tests/util.