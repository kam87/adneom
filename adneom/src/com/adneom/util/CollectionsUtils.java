package com.adneom.util;

import java.util.ArrayList;
import java.util.List;

public class CollectionsUtils {
	/**
	 * 
	 * @param list la liste a decoup�
	 * @param sizeSousList la taille de la sous liste
	 * @return Une liste de sous liste 
	 */
	public static List<List> partition(List list, int sizeSousList) {
		if (list != null && sizeSousList>0) {
			List<List> listResultat = new ArrayList<List>();
			int indexSousList = 0;
			List sousList = new ArrayList<>();
			for (int index = 0; index < list.size(); index++) {
				sousList.add(list.get(index));
				indexSousList++;
				if (indexSousList == sizeSousList || index+1==list.size()) {
					listResultat.add(sousList);
					sousList = new ArrayList<>();
					indexSousList = 0;
				}
			}
			return listResultat;
		}
		return null;
	}

}
