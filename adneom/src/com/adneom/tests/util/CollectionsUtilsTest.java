package com.adneom.tests.util;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import com.adneom.util.CollectionsUtils;

public class CollectionsUtilsTest {


	/**
	 * 
	 * @return une liste de sous liste de taille 2
	 */
	public List<List<Integer>> getListSizeTow() {
		List<List<Integer>> list = new ArrayList<List<Integer>>();
		list.add(new ArrayList<Integer>(Arrays.asList(1, 2)));
		list.add(new ArrayList<Integer>(Arrays.asList(3, 4)));
		list.add(new ArrayList<Integer>(Arrays.asList(5)));

		return list;
	}
	
	/**
	 * 
	 * @return une liste de sous liste de taille 3
	 */
	public List<List<Integer>> getListSizeTree() {
		List<List<Integer>> list = new ArrayList<List<Integer>>();
		list.add(new ArrayList<Integer>(Arrays.asList(1,2,3)));
		list.add(new ArrayList<Integer>(Arrays.asList(4, 5)));
		return list;
	}

	/**
	 * 
	 * @return une liste de sous liste de taille 5
	 */
	public List<List<Integer>> getListSizeFive() {
		List<List<Integer>> list = new ArrayList<List<Integer>>();
		list.add(new ArrayList<Integer>(Arrays.asList(1,2,3,4, 5)));
		return list;
	}
	
	@Test
	public void partitionListTest() {
		List<Integer> data = new ArrayList<Integer>(Arrays.asList(1, 2, 3, 4, 5));
		List<List> sublists = CollectionsUtils.partition(data, 2);
		assertEquals(getListSizeTow(), sublists);
		List<List> sousList = CollectionsUtils.partition(data, 3);
		assertEquals(getListSizeTree(), sousList);
	}
	
	@Test
	public void invalideSizeSousListTest() {
		List<Integer> listData = new ArrayList<Integer>(Arrays.asList(1, 2, 3, 4, 5));
		List<List> sublists = CollectionsUtils.partition(listData, -5);
		assertNull(sublists);
		
	}
	
	@Test
	public void bigSizeSousListTest() {
		List<Integer> data = new ArrayList<Integer>(Arrays.asList(1, 2, 3, 4, 5));
		List<List> sublists = CollectionsUtils.partition(data, 6);
		assertEquals(getListSizeFive(), sublists);
	 
	}
	
	@Test
	public void emptyListTest() {
		List<Integer> listData = new ArrayList<Integer>(Arrays.asList());
		List<List> sublists = CollectionsUtils.partition(listData, 2);
		assertEquals(listData, sublists);
	}

	@Test
	public void nullListTest() {
		List<List> sublists = CollectionsUtils.partition(null, 2);
		assertNull(sublists);
	}
	
	@Test
	public void classTest() {
		List<Integer> listData = new ArrayList<Integer>(Arrays.asList(1, 2, 3, 4, 5));
		List<List> sublists = CollectionsUtils.partition(listData, 1);
		assertEquals(5, sublists.size());
		for (List sublist : sublists) {
			assertTrue(sublist instanceof List);
		}
	}

}

